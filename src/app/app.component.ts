import { Component } from '@angular/core';
import { count } from 'rxjs';

interface DataExportText {
  content: string
  name: string
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'check_sequence';
  list: any = []
  text: any = []

  list2: any = []
  text2: any = []

  monitorSequence: any = {
    total: 0,
    id: [],
    shuffle: 0,
    shuffleId: [],
    drop: 0,
    dropId: [],
  }

  vttText: DataExportText =  {
    content: '',
    name: ''
  };



  mergeText: string = ''
  selectFile(event: any) {
    var [file] = event.target.files
    let fileReader = new FileReader();
    fileReader.onload = (e) => {

      var content: any = {}
      var result = fileReader.result?.toString();

      var arrResult = result?.split('\n')
      var regTest = /(drop frame: )/
      var reg = /^[0-9]*$/
      var regV2 = /^[0-9]*(\t){1}[0-9]*/
      var regText = /^\({1}/
      var index = 0
      arrResult = arrResult?.map(e => {
        if(regTest.test(e)) {
          e = e.replace(regTest, '')
          var count = e.length / 2;
          e = e.slice(0, count);
        }
        return e
      })
      arrResult?.forEach((value:any) => {
        if(regV2.test(value)) {
          var [chunkId, valueIndex] = value.split('\t')
          if(!content[`${chunkId}`]) {
            content[`${chunkId}`] = {
              sequence: true,
              indexWrongSequence: [],
              offset:0,
              offsetId:0,
              value: [],
            }
            index = 0
          } else {
            index++
          }
          if(index != valueIndex) {
            content[`${chunkId}`].sequence = false
            content[`${chunkId}`].indexWrongSequence.push(valueIndex)
           
            content[`${chunkId}`].value.push({sequence: valueIndex, correct: false})
          } else {
            content[`${chunkId}`].value.push({sequence: valueIndex, correct: true})
          }
        } else if (regText.test(value)){
          
        } else {

          this.text.push(value)
        }
      
      })
      
      var totalAll = 0
      var totalShuffleAll = 0;
      Object.keys(content).forEach(index => {
        var list = content[`${index}`].value.map((e:any, index: number) => {
          return {
            id: e.sequence,
            offset: Math.abs(parseInt(e.sequence) - index)
          }
        })
        var shuffleList: any = [];
        var totalShuffle = 0
        var current = 0;
        content[`${index}`].value.forEach((e: any, index: number) => {        
            if(index == 0) {
              current = parseInt(e.sequence)
            } else {
              if(current > parseInt(e.sequence)) {
                shuffleList.push(parseInt(e.sequence))
                totalShuffle++;
              } else {
                current = parseInt(e.sequence)
              }
            }
        })

 
      
       var max = Math.max(...list.map((e:any) => e.offset))
       var maxId = list.find((e:any) => e.offset == max)
        this.list.push({msgId: (parseInt(index) + 1) ,maxOffsetId: maxId.id ,maxOffset: max, value: content[`${index}`], 
        shuffle: {
          list: shuffleList,
          total: totalShuffle
        }})
        totalAll += content[`${index}`].value.length
        totalShuffleAll += totalShuffle
        
      })
      console.log(totalAll,totalShuffleAll);
      this.list.forEach((e:any)=> {
        if(!e.value.sequence) {
          this.monitorSequence.shuffle++;
          this.monitorSequence.shuffleId.push(e.msgId)
        }
      })
      if(this.list2.length != 0) {
        this.monitor()
      }
    }
    fileReader.readAsText(file);
    
  }


  selectFile2(event: any) {
    var [file] = event.target.files
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var msgId = 0
      var content: any = {}
      var result = fileReader.result?.toString();

      var arrResult = result?.split('\n')
      var regTest = /(drop frame: )/
      var regText = /^\({1}/
      var reg = /^[0-9]*$/
      var regV2 = /^[0-9]*(\t){1}[0-9]*/
      var index = 0

      arrResult = arrResult?.map(e => {
        if(regTest.test(e)) {
          e = e.replace(regTest, '')
          var count = e.length / 2;
          e = e.slice(0, count);
        }
        return e
      })
      arrResult?.forEach((value:any) => {
        console.log(value)
        if(regV2.test(value)) {
          var [chunkId, valueIndex] = value.split('\t')
          if(!content[`${chunkId}`]) {
            content[`${chunkId}`] = {
              sequence: true,
              indexWrongSequence: [],
              offset: 0,
              offsetId: 0,
              value: []
            }
            index = 0
          } else {
            index++
          }
         
          if(index != valueIndex) {
            content[`${chunkId}`].sequence = false
            content[`${chunkId}`].indexWrongSequence.push(valueIndex)
            var offset = (valueIndex - index)
            if(offset > content[`${chunkId}`].offset) {
              content[`${chunkId}`].offset = offset
              content[`${chunkId}`].offsetId = valueIndex
            }
            content[`${chunkId}`].value.push({sequence: valueIndex, correct: false})
          } else {
            content[`${chunkId}`].value.push({sequence: valueIndex, correct: true})
          }
        } else if (regText.test(value)){
          
        } else {
          this.text2.push(value)
        }
      
      })
      

     
      Object.keys(content).forEach(index => {
        this.list2.push({msgId: (parseInt(index) + 1), value: content[`${index}`]})
      })

      this.list2.forEach((e:any)=> {
        if(!e.value.sequence) {
          this.monitorSequence.drop++;
          this.monitorSequence.dropId.push(e.msgId)
        }
      })

      if(this.list.length != 0) {
        this.monitor()
      }
    }
    fileReader.readAsText(file);

   
  }


  selectMergeFile(event: any) {
    this.mergeText = ''
    var files: File[] = event.target.files
    var groupFile :File[] = []
    var list:any[] = [];
    var regEx = /text/
    for (let i=0; i<files.length;i++) {
      
        if(regEx.test(files[i].name)) {
          groupFile.push(files[i])
        }
    }
    console.log(groupFile)
    for (let i=0; i<groupFile.length;i++) {
      let fileReader = new FileReader();
      fileReader.onload = (e) => {
        var result = fileReader.result?.toString();
        var [id] = groupFile[i].name.split('_')
        list.push({
          name: id,
          text: result
        })
      }
      fileReader.readAsText(groupFile[i])
    }
    setTimeout(() => {
      list.sort((a,b) => a.name - b.id).forEach(e => this.mergeText += e.text)
    }, 1000)

  }

  exportText(body:string , filename: string) {
    let file;
    let downloadLink;
    file = new Blob(['\uFEFF' + body], { type: 'text/plain;charset=utf-8' });
    downloadLink = document.createElement('a');
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(file);
    downloadLink.style.display = 'none';
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  selectVTTtoTxt(event: any) {
    var [file]: File[] = event.target.files
    this.RenderFileToString(file).then((result:any) => {
      console.log(result)
      var arrResult: string[] = result.split('\r\n')
      var regEx = /-->/
      var listText = arrResult.filter((e:string) => !regEx.test(e) && e)
      this.vttText = {
        content: listText.join('\n'),
        name: 'text_' + file.name.replace('vtt', 'txt')
      }
    }).catch(err => {
      alert(err);
    })

  }


  monitor() {
    this.list.forEach((value: any, index: number) => {
      if(this.list2[index]) {
        if((value.value.sequence == this.list2[index].value.sequence) && !value.value.sequence ) {
          this.monitorSequence.total++
          this.monitorSequence.id.push(value.msgId)
        }
      }
    })
    console.log()
  }

  exportVTTtoTxt() {
    this.exportText(this.vttText.content, this.vttText.name)
  }

  RenderFileToString(file: File) {
    return new Promise((resolve, reject) => {

        let fileReader = new FileReader();
        fileReader.onload = (e) => {
          resolve(fileReader.result?.toString())
        }
        fileReader.onerror = (e) => {
          reject('file not found')
        }
        fileReader.readAsText(file)

   
    })
  }
}
